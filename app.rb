Dir["#{File.dirname(__FILE__)}/lib/*.rb"].sort.each { |file| require file }

require 'sinatra'

class FitbitEb < Sinatra::Base
  get '/' do
    erb :index
  end
end